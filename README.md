# Hangman Online

Play hangman with your friends across the globe!

## Getting started

Compile the project. Make sure [SFML](https://www.sfml-dev.org/) and [nlohmann-json](https://github.com/nlohmann/json) libraries are installed on the system.
```
make
```

Run the server application and the client applications:
```
./hangman-online-server

./hangman-online
./hangman-online
...
./hangman-online
```
