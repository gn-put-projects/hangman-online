MAJOR=0
MINOR=1
VERSION=$(MAJOR).$(MINOR)

### Paths for source and object files
SRC=src
# Server source files
S=$(SRC)/server
# Client source files
C=$(SRC)/client
# Extras, shared source files
E=$(SRC)/shared

OBJ=$(SRC)/obj
# Server object files
SO=$(OBJ)/server
# Client object files
CO=$(OBJ)/client
# Extras, shared object files
EO:=$(OBJ)/shared

# List of paths to shared object files
SHARED_OBJS:=$(patsubst $E%.cpp,$(EO)%.o,$(wildcard $E/*.cpp))

# List of paths to client object files
CLIENT_OBJS:=$(patsubst $C%.cpp,$(CO)%.o,$(wildcard $C/*.cpp))

# List of paths to server object files
SERVER_OBJS:=$(patsubst $S%.cpp,$(SO)%.o,$(wildcard $S/*.cpp))

### Compiler and compiler flags
CC=g++
CFLAGS=-std=c++20 -Wall -Wextra -g
OBJ_CFLAGS=-c $(CFLAGS)

### Used libraries
LIBSFML=-lsfml-graphics -lsfml-window -lsfml-system

CLIENTLIBS=$(LIBSFML)
SERVERLIBS=$()

.PHONY: test compile clean

compile: hangman-online hangman-online-server

test-server: hangman-online-server
	./hangman-online-server

test-client: hangman-online
	./hangman-online

# Client application
hangman-online: $(CLIENT_OBJS) $(SHARED_OBJS)
	$(CC) $(CFLAGS) $^ -o $@ $(CLIENTLIBS)

# Server application
hangman-online-server: $(SERVER_OBJS) $(SHARED_OBJS)
	$(CC) $(CFLAGS) $^ -o $@ $(SERVERLIBS)

$(OBJ)/%.o: $(SRC)/%.cpp $(SRC)/%.h
	$(CC) $(OBJ_CFLAGS) $< -o $@

$(OBJ)/%.o: $(SRC)/%.cpp
	$(CC) $(OBJ_CFLAGS) $< -o $@

clean:
	rm -f hangman-online hangman-online-server $(SO)/*.o $(CO)/*.o $(EO)/*.o
