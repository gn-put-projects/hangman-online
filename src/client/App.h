#ifndef APP_H
#define APP_H

#include <iostream>
#include <SFML/Graphics.hpp>
#include <memory>
#include <stack>
#include <vector>
#include <thread>
#include <chrono>

class App;

#include "GameUpdater.h"
#include "Client.h"
#include "Stage.h"
#include "Widget.h"
#include "TextField.h"

/**
   The App's engine, responsible for updating the state
   of the client application as well as for graphics
   in the app window.
*/
class App {
private:
  std::unique_ptr<GameUpdater> gameUpdater;
  std::shared_ptr<sf::RenderWindow> window;
  /**
     A stack holding the opened Stages IDs (currently visible
     Stage at the top).
   */
  std::stack<int> stageStack;

  std::unique_ptr<Stage> mainMenu;
  std::unique_ptr<Stage> connectMenu;
  std::unique_ptr<Stage> lobbyStage;
  std::unique_ptr<Stage> popupStage;
  std::unique_ptr<Stage> gameStage;
  std::unique_ptr<Stage> scoresStage;
  
  sf::Event event;
  sf::VideoMode windowSize;
  std::string windowName;
  
  void manageEvents();
  void clearEvents();
  
  void update();
  void render();

  void initSFML();
  void setupSFML();

  void createStages();

  std::unique_ptr<Stage>& topStage();
public:
  int getRound();
  void showSingleImage(int);
  void processMessage(std::string);
  std::unique_ptr<Client> client;
  void popUp(std::string);
  void pushStage(int);
  void popStage();
  void goToBottomStage();
  void sendMessage(json);
  std::string getInput(std::string);
  std::string getWidgetInput(std::string);
  Widget& getWidget(std::string);
  TextField& getTextField(std::string);
  void connectToServer(std::string, std::string);
  int currentStage();
  void resetCurrentStage();
  std::shared_ptr<sf::Font> font;
  void loop();
  App();
  ~App();
};

#endif
