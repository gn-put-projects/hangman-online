#ifndef WIDGET_H
#define WIDGET_H

#include <iostream>
#include <SFML/Graphics.hpp>
#include <memory>
#include <functional>
#include <error.h>

#define WIDGET_COLOR sf::Color(50, 50, 100)
#define WIDGET_HOVER_COLOR sf::Color(70, 70, 140)
#define DISABLED_ALPHA 100

class Widget;

// Forward declaration for Stage* widget field
class Stage;

class Widget {
protected:
  bool disabled;
  std::shared_ptr<sf::RenderWindow> window;
  int width;
  int height;
  int x;
  int y;
  sf::RectangleShape rect;
  std::function<void(Stage*)> callback;
  std::shared_ptr<sf::Font> font;
  int fontSize;
  bool textSet;
  sf::Color fontColor;
  sf::Text sfText;
  
  void positionText();
  
public:
  bool hidden;
  bool textCentered;
  bool textCenteredX;
  bool textCenteredY;
  float textPaddingX;
  float textPaddingY;
  // Disable and enable the widget
  bool isDisabled();
  void disable();
  void enable();
  // Disable widget's hover color change
  bool nohover;
  // Disable widget's background
  bool disableRect;
  std::string text;
  std::string name;
  Stage* stage;
  Widget(Stage*, std::shared_ptr<sf::RenderWindow>,
	 int, int);
  void render();
  void setOnClick(std::function<void(Stage*)>);
  virtual void manageLeftClick(int, int);
  void manageMouseMove(int, int);
  void setFont(std::shared_ptr<sf::Font>, int=20, sf::Color=sf::Color::White);
  void setText(std::string);
  void setId(std::string);
  void setPosition(sf::Vector2f);
  sf::FloatRect getLocalBounds();
  void setColor(sf::Color);
  ~Widget();
};

#endif
