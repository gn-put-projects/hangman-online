#include "Client.h"

Client::Client(App* appP) {
  this->app = appP;
  this->errorMsg = "";
}

void Client::disconnect() {
  close(this->sock);
  close(this->eFd);
}

bool Client::connectToServer(std::string ipAddress) {
  addrinfo* resolved;
  addrinfo hints= {
    .ai_flags=0,
    .ai_family=AF_INET,
    .ai_socktype=SOCK_STREAM,
    .ai_protocol=IPPROTO_TCP,
    .ai_addrlen=0,
    .ai_addr=nullptr,
    .ai_canonname=nullptr,
    .ai_next=nullptr
  };
  
  int gaiOut = getaddrinfo(ipAddress.c_str(), "9191", &hints, &resolved);
  if (gaiOut || !resolved) {
    this->errorMsg = std::string(gai_strerror(gaiOut));
    return false;
  }
    	
  this->sock = socket(resolved->ai_family, resolved->ai_socktype, 0);
  if (this->sock == -1) {
    this->errorMsg = "Socket creation error";
    return false;
  }

  int fcntlOut = fcntl(this->sock, F_SETFL,
		       fcntl(this->sock, F_GETFL, 0) | O_NONBLOCK);

  if (fcntlOut == -1)
    error(1, errno, "Fnctl failed (setting client fd to NONBLOCKing mode)");

  bool connected = false;
  
  int connectOut = connect(sock, resolved->ai_addr, resolved->ai_addrlen);
  if (connectOut == -1) {
    if (errno != EINPROGRESS)
      error(1, errno, "connect");
  }
  else connected = true;
  
  // free memory
  freeaddrinfo(resolved);

  this->eFd = epoll_create1(0);

  epoll_event event;

  if (!connected) {
    event.events = EPOLLOUT;
    event.data.fd = this->sock;
    epoll_ctl(this->eFd, EPOLL_CTL_ADD, this->sock, &event);

    int n = epoll_wait(this->eFd, &event, 1, 3000);
    if (n < 1) {
      this->errorMsg = "Timed out after trying for 3 seconds";
      return false;
    }
    epoll_ctl(this->eFd, EPOLL_CTL_DEL, this->sock, nullptr);

    // Get the error message and report.
    int so_error = 0;
    socklen_t so_error_size = sizeof(so_error);
    int getSockOptOut =
      getsockopt(this->sock, SOL_SOCKET, SO_ERROR, &so_error, &so_error_size);
    if (getSockOptOut == -1) {
      error(1, errno, "getsockopt");
    }
    if (so_error != 0) {
      this->errorMsg = std::string(strerror(so_error));
      return false;
    }
  }
 
  event.events = EPOLLIN;
  
  event.data.fd = this->sock;
  epoll_ctl(this->eFd, EPOLL_CTL_ADD, this->sock, &event);

  this->readWriter = std::make_unique<ReadWriter>(this->sock, this->eFd);
  
  return true;
}

void Client::sendMessage(std::string msg) {
  if (this->readWriter.get() == nullptr)
    error(1, 0, "Attempt to send a message without a connection");
  this->readWriter->requestWrite(msg);
}

void Client::update() {
  epoll_event event;

  int n = epoll_wait(this->eFd, &event, 1, 0);
  if (n < 1) return;

  if (event.events & EPOLLIN && event.data.fd == this->sock) {
    bool read = this->readWriter->readNow();    
    
    if (!read) {
      // server stopped responding
      this->disconnect();
      this->app->goToBottomStage();
      this->app->popUp("Lost connection to server");
    }

    std::string message = this->readWriter->receiveMessage();
    this->app->processMessage(message);
    return;
  }

  if (event.events & EPOLLOUT) {
    this->readWriter->writeNow();
    return;
  }

  std::cout<<"Unrecognized epoll event!\n";
}

Client::~Client() {
  close(this->sock);
  close(this->eFd);
}
