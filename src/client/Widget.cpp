#include "Widget.h"

void Widget::setPosition(sf::Vector2f pos) {
  this->x = pos.x;
  this->y = pos.y;
  this->rect.setPosition(this->x, this->y);
  this->positionText();
}

bool Widget::isDisabled() {
  return this->disabled;
}

void Widget::disable() {
  this->disabled = true;
  this->setColor(WIDGET_COLOR);
  this->setFont(this->font, this->fontSize, this->fontColor);
}

void Widget::enable() {
  this->disabled = false;
  this->setColor(WIDGET_COLOR);
  this->setFont(this->font, this->fontSize, this->fontColor);
}

void Widget::setColor(sf::Color color) {
  if (this->disabled) {
    this->rect.setFillColor
      (sf::Color(color.r, color.g, color.b, DISABLED_ALPHA));
    return;
  }
  this->rect.setFillColor(color);
}
  
sf::FloatRect Widget::getLocalBounds() {
  return sf::Rect<float>(x, y, width, height);
}

void Widget::positionText() {
  if (!textSet) return;
  float textX = x + textPaddingX * width;
  float textY = y + textPaddingY * height;
  if (textCentered && textCenteredX) {
    textX = x + (width - sfText.getLocalBounds().width) / 2;
  }
  if (textCentered && textCenteredY) {
    textY = y + (height - sfText.getLocalBounds().height) / 2;
    textY += font->getGlyph('A', fontSize, false).bounds.top / 2;
  }
  sfText.setPosition(textX, textY);
}

Widget::Widget(Stage* stage, std::shared_ptr<sf::RenderWindow> win,
	       int w, int h) {
  this->stage = stage;
  this->window = win;
  this->x = 0;
  this->y = 0;
  this->width = w;
  this->height = h;
  
  this->rect = sf::RectangleShape(sf::Vector2f(this->width, this->height));
  this->rect.setFillColor(WIDGET_COLOR);
  
  this->rect.setPosition(this->x, this->y);

  this->fontSize = 20;
  this->textSet = false;
  this->text = "unset";

  this->textPaddingX = 0.05;
  this->textPaddingY = 0;

  this->hidden = false;
  this->textCentered = true;
  this->textCenteredX = true;
  this->textCenteredY = true;
  
  this->nohover = false;
  this->disableRect = false;
  this->disabled = false;
}

void Widget::render() {
  if (this->hidden) return;
  if (!this->disableRect)
    this->window->draw(this->rect);
  this->window->draw(this->sfText);
}

void Widget::setFont(std::shared_ptr<sf::Font> newFont,
		     int size,
		     sf::Color color) {
  this->font = newFont;
  this->fontSize = size;
  this->sfText = sf::Text(this->text, *this->font, this->fontSize);

  if (!this->disabled) {
    sfText.setFillColor(color);
  }
  else {
    sfText.setFillColor
      (sf::Color(color.r, color.g,
		 color.b, DISABLED_ALPHA));
  }
  
  this->fontColor = color;
  
  this->textSet = true;
  this->positionText();
}

void Widget::setText(std::string newText) {
  this->text = newText;
  this->sfText.setString(newText);
  this->positionText();
}

/**
   Check if the widget has been pressed.
   If so, run the widget's callback.
*/
void Widget::manageLeftClick(int mx, int my) {
  if (this->disabled || this->hidden) return;
  if (mx > this->x && mx < this->x+this->width &&
      my > this->y && my < this->y+this->height) {
    if (this->callback) {
      this->setColor(WIDGET_COLOR);
      this->callback(this->stage);
    }
  }
}

/**
   Adjust the widget's appearance according to whether
   the mouse hovered over it.
*/
void Widget::manageMouseMove(int mx, int my) {
  if (mx > this->x && mx < this->x+this->width &&
      my > this->y && my < this->y+this->height &&
      !this->nohover && !this->disabled) {
    this->setColor(WIDGET_HOVER_COLOR);
  }
  else this->setColor(WIDGET_COLOR);
}

void Widget::setOnClick(std::function<void(Stage*)> cb) {
  this->callback = std::move(cb);
}

void Widget::setId(std::string id) {
  this->name = id;
}

Widget::~Widget() {
}
