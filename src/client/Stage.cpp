#include "Stage.h"

#define PERIODS_IN_IP_ADDR 3

template <class T>
void Stage::centerElement(T& el, float posY) {
  float posX = (this->window->getSize().x - el.getLocalBounds().width) / 2;
  el.setPosition(sf::Vector2f(posX, posY));
}

void Stage::setEventHook(std::function<bool(sf::Event)> hook) {
  this->eventHook = std::move(hook);
}

void Stage::addImage(std::string path) {
  this->textures.emplace_back(std::make_unique<sf::Texture>());
  if (!this->textures.back()->loadFromFile(path)) {
    error(1, 0, "Failed to load texture");
  }
  this->sprites.emplace_back();
  this->showSprites.emplace_back(true);
  this->sprites.back().setTexture(*this->textures.back().get());
}

void Stage::hideAllImages() {
  for (uint i=0; i< this->showSprites.size(); ++i) {
    this->showSprites.at(i) = false;
  }
}

void Stage::setImageVisibility(int index, bool visible) {
  this->showSprites.at(index) = visible;
}

sf::Sprite& Stage::getImage(int index) {
  return this->sprites.at(index);
}

/**
   Resets this stage to the starting state
*/
void Stage::resetStage() {
  if (stageIndex == STAGE_LOBBY) {
    this->getWidget("player-widget").setText("");
    this->getWidget("viewer-widget").setText("");
    this->getWidget("lobby-state").setText("Getting lobby data...");
  }
  if (stageIndex == STAGE_GAME) {
    for (uint i = 0; i<26; i++) {
      std::string letter = std::string(1, static_cast<char>(i+'A'));
      this->getWidget(letter + "-widget").hidden = false;
      this->getWidget(letter + "-widget").enable();
    }
    this->getWidget("covered-word").setText("");
    this->getTextField("guess-text").setText("Guess here!");
    this->getTextField("guess-text").reset();
    this->getWidget("guess-widget").disable();
    this->getWidget("viewer-info").hidden = true;
    this->getTextField("guess-text").hidden = false;
    this->getWidget("guess-widget").hidden = false;
    this->getWidget("time-text").hidden = false;
  }
}

/**
   Sets up a hardcoded stage (like the main menu, game screen, etc.)
*/
void Stage::setupStage(int givenStageIndex) {
  stageIndex = givenStageIndex;
  if (stageIndex == STAGE_MENU) {
    // Game title
    texts.emplace_back("Hangman Online", *this->app->font, 120);
    texts.back().setFillColor(sf::Color::White);
    centerElement(texts.back(), 0);
    
    // Play widget
    widgets.emplace_back(this, this->window, 270, 170);
    widgets.back().setFont(this->app->font, 60);
    widgets.back().setText("PLAY");
    centerElement(widgets.back(), 200);
    widgets.back().setOnClick([this](Stage*) {
      this->app->pushStage(STAGE_CONNECT);
    });

    // Hangman image
    this->addImage("res/images/hangman-11.png");
    this->getImage(0).setPosition(sf::Vector2f(20, 250));
    
    return;
  }
  if (stageIndex == STAGE_CONNECT) {
    // Title
    texts.emplace_back("Connect to a game", *this->app->font, 70);
    texts.back().setFillColor(sf::Color::White);
    centerElement(texts.back(), 0);

    // IP Address label
    texts.emplace_back("Server's IP address", *this->app->font, 30);
    texts.back().setFillColor(sf::Color::White);
    texts.back().setPosition(sf::Vector2f(550, 160));
    
    // IP Address text field
    textFields.emplace_back(this, "ip-address", this->window, 500, 80);
    textFields.back().setFont(this->app->font, 50);
    textFields.back().textPaddingY = 0.08;
    textFields.back().alphaEnabled = false;
    textFields.back().spaceEnabled = false;
    centerElement(textFields.back(), 200);
    textFields.back().setText("127.0.0.1");
    textFields.back().maxTextLength = 15;
    textFields.back().setOnInput([this](TextField* self, char c) {
      std::string t = self->text;
      if (c == '.') {
	if (t.back() != '.' && std::ranges::count(t, '.') < PERIODS_IN_IP_ADDR)
	  self->defaultAddChar('.');
	return;
      }
      if (t.size() > 2
	  && t.substr(t.size()-3, 3).find('.') == std::string::npos) {
	return;
      }
      self->defaultAddChar(c);
    });

    // Nickname label
    texts.emplace_back("Nickname", *this->app->font, 30);
    texts.back().setFillColor(sf::Color::White);
    texts.back().setPosition(sf::Vector2f(550, 300));
    
    // Nickname text field
    textFields.emplace_back(this, "nickname", this->window, 500, 80);
    textFields.back().setFont(this->app->font, 60);
    textFields.back().maxTextLength = 12;
    textFields.back().eraseOnFirstFocus = true;
    centerElement(textFields.back(), 340);
    textFields.back().setText("nickname");
    textFields.back().setOnInput([this](TextField* self, char c) {
      self->defaultAddChar(c);
      if (self->text.length())
	this->getWidget("connect-widget").enable();
      else
	this->getWidget("connect-widget").disable();
    });
    
    widgets.emplace_back(this, this->window, 100, 30);
    widgets.back().setFont(this->app->font, 25);
    widgets.back().setText("back");
    widgets.back().setOnClick([this](Stage*) {
      this->app->client->disconnect();
      this->app->popStage();
    });

    // Connect widget
    widgets.emplace_back(this, this->window, 270, 100);
    widgets.back().setFont(this->app->font, 50);
    widgets.back().setId("connect-widget");
    widgets.back().setText("Connect");
    centerElement(widgets.back(), 600);
    widgets.back().setOnClick([this](Stage* stage) {
      std::string nickname = stage->getTextField("nickname").text;
      if (nickname == "") return;
      std::string ipAddress = stage->getTextField("ip-address").text;
      app->connectToServer(ipAddress, nickname);
    });
    
    return;
  }
  if (stageIndex == STAGE_LOBBY) {
    // Title
    texts.emplace_back("Lobby", *this->app->font, 70);
    texts.back().setFillColor(sf::Color::White);
    centerElement(texts.back(), 0);

    // Role selection
    texts.emplace_back("Role selection:", *this->app->font, 30);
    texts.back().setFillColor(sf::Color::White);
    texts.back().setPosition(sf::Vector2f(550, 160));

    texts.emplace_back("Player", *this->app->font, 18);
    texts.back().setFillColor(sf::Color::White);
    texts.back().setPosition(sf::Vector2f(550, 215));

    texts.emplace_back("Viewer", *this->app->font, 18);
    texts.back().setFillColor(sf::Color::White);
    texts.back().setPosition(sf::Vector2f(648, 215));
    
    // Player widget
    widgets.emplace_back(this, this->window, 50, 50);
    widgets.back().setId("player-widget");
    widgets.back().setFont(this->app->font, 20);
    widgets.back().setText("");
    widgets.back().setPosition(sf::Vector2f(550, 240));
    widgets.back().setOnClick([this](Stage* stage) {
      if (stage->getWidget("viewer-widget").text == "..." ||
	  stage->getWidget("player-widget").text != "")
	return;
      stage->getWidget("player-widget").setText("...");
      
      json msg;
      msg["type"] = USER_ROLE_DECLARED;
      msg["role"] = ROLE_PLAYER;
      
      app->sendMessage(msg);
    });

    // Viewer widget
    widgets.emplace_back(this, this->window, 50, 50);
    widgets.back().setId("viewer-widget");
    widgets.back().setFont(this->app->font, 20);
    widgets.back().setText("");
    widgets.back().setPosition(sf::Vector2f(650, 240));
    widgets.back().setOnClick([this](Stage* stage) {
      if (stage->getWidget("player-widget").text == "..." ||
	  stage->getWidget("viewer-widget").text != "")
	return;
      stage->getWidget("viewer-widget").setText("...");
      json msg;
      msg["type"] = USER_ROLE_DECLARED;
      msg["role"] = ROLE_VIEWER;
      
      app->sendMessage(msg);
    });
    
    widgets.emplace_back(this, this->window, 100, 30);
    widgets.back().setFont(this->app->font, 25);
    widgets.back().setText("exit");
    widgets.back().setOnClick([this](Stage*) {
      app->client->disconnect();
      this->app->goToBottomStage();
    });

    
    // Lobby state text
    widgets.emplace_back(this, this->window, 370, 100);
    widgets.back().disableRect = true;
    widgets.back().setId("lobby-state");
    widgets.back().setFont(this->app->font, 40);
    widgets.back().setText("Getting lobby data...");
    centerElement(widgets.back(), 400);
    
    // Ranking text
    widgets.emplace_back(this, this->window, 100, 100);
    widgets.back().setId("lobby-players");
    widgets.back().disableRect = true;
    widgets.back().textCentered = false;
    widgets.back().textPaddingX = 0;
    widgets.back().setFont(this->app->font, 22);
    widgets.back().setText("");
    widgets.back().setPosition(sf::Vector2f(15, 150));

    // Ready widget
    widgets.emplace_back(this, this->window, 370, 100);
    widgets.back().setId("ready-widget");
    widgets.back().setFont(this->app->font, 50);
    widgets.back().setText("READY");
    centerElement(widgets.back(), 600);
    widgets.back().setOnClick([this](Stage* stage) {
      if (stage->getWidget("ready-widget").text == "...")
	return;
      json msg;
      msg["type"] = USER_READY_CHANGE;
      if (stage->getWidget("ready-widget").text == "READY")
	msg["ready"] = true;
      else msg["ready"] = false;
      
      app->sendMessage(msg);
    });
    return;
  }
  if (stageIndex == STAGE_POPUP) {
    this->setEventHook([this](sf::Event e) -> bool {
      if ((e.type == sf::Event::MouseButtonPressed
	   && e.mouseButton.button == sf::Mouse::Button::Left)
	  || e.type == sf::Event::KeyPressed) {
	this->app->popStage();
	return false;
      }
      return true;
    });
    // Message display
    widgets.emplace_back(this, this->window, 800, 400);
    widgets.back().setId("message");
    widgets.back().nohover = true;
    widgets.back().setFont(this->app->font, 30);
    widgets.back().setText("No message was set for this popup.");
    centerElement(widgets.back(), 300);
    return;
  }
  if (stageIndex == STAGE_GAME) {
    // Player's score
    widgets.emplace_back(this, this->window, 100, 100);
    widgets.back().setId("score-text");
    widgets.back().disableRect = true;
    widgets.back().setFont(this->app->font, 70);
    widgets.back().setText("Score: 450");
    centerElement(widgets.back(), 15);
            
    // Time left
    widgets.emplace_back(this, this->window, 100, 100);
    widgets.back().setId("time-text");
    widgets.back().disableRect = true;
    widgets.back().setFont(this->app->font, 70);
    widgets.back().setText("17s");
    widgets.back().textCentered = false;
    widgets.back().textPaddingX = 0;
    widgets.back().setPosition(sf::Vector2f(15, 15));
    
    // Hangman image
    for (uint i=0; i<=11; ++i) {
      std::string imgNumber = std::to_string(i);
      this->addImage("res/images/hangman-"+imgNumber+".png");
      this->getImage(i).setPosition(sf::Vector2f(1385, 15));
    }
    this->hideAllImages();

    // Ranking text
    widgets.emplace_back(this, this->window, 100, 100);
    widgets.back().setId("ranking");
    widgets.back().disableRect = true;
    widgets.back().textCentered = false;
    widgets.back().textPaddingX = 0;
    widgets.back().setFont(this->app->font, 22);
    widgets.back().setText("");
    widgets.back().setPosition(sf::Vector2f(15, 150));
    
    // Notification text
    widgets.emplace_back(this, this->window, 400, 70);
    widgets.back().setId("notification");
    widgets.back().disableRect = true;
    widgets.back().textCentered = false;
    widgets.back().textPaddingX = 0;
    widgets.back().setFont(this->app->font, 25);
    widgets.back().setText("");
    widgets.back().setPosition(sf::Vector2f(100, 630));

    // Last guess text
    widgets.emplace_back(this, this->window, 200, 70);
    widgets.back().setId("lastguess");
    widgets.back().disableRect = true;
    widgets.back().textCentered = false;
    widgets.back().textPaddingX = 0;
    widgets.back().setFont(this->app->font, 50);
    widgets.back().setText("");
    widgets.back().setPosition(sf::Vector2f(850, 680));
        
    // Word guess field
    textFields.emplace_back(this, "guess-text", this->window, 450, 70);
    textFields.back().setFont(this->app->font, 40);
    textFields.back().maxTextLength = 15;
    textFields.back().textPaddingY = 0.08;
    textFields.back().eraseOnFirstFocus = true;
    textFields.back().setPosition(sf::Vector2f(850, 780));
    textFields.back().setText("Guess here!");
    textFields.back().numbersEnabled = false;
    textFields.back().spaceEnabled = false;
    textFields.back().setOnInput([this](TextField* self, char c) {
      self->defaultAddChar(c);
      if (self->text.length())
	this->getWidget("guess-widget").enable();
      else
	this->getWidget("guess-widget").disable();
    });
    
    //Word guess widget
    widgets.emplace_back(this, this->window, 200, 70);
    widgets.back().setId("guess-widget");
    widgets.back().setFont(this->app->font, 50);
    widgets.back().setText("GUESS");
    widgets.back().setPosition(sf::Vector2f(1350, 780));
    widgets.back().disable();
    widgets.back().setOnClick([this](Stage* stage) {
      std::string guess = stage->getTextField("guess-text").text;
      if (guess == "") return;
      
      json msg;
      msg["type"] = USER_WORD;
      msg["guess"] = guess;
      msg["round"] = app->getRound();
      
      app->sendMessage(msg);
    });
    
    // Word to guess
    widgets.emplace_back(this, this->window, 100, 100);
    widgets.back().setId("covered-word");
    widgets.back().disableRect = true;
    widgets.back().setFont(this->app->font, 120);
    widgets.back().setText("");
    centerElement(widgets.back(), 300);
    
    // Letter widgets
    std::function<void(std::string, int, int)> mkLetterWidget =
      [this](std::string letter, int x, int y){
	widgets.emplace_back(this, this->window, 50, 50);
	widgets.back().setId(letter + "-widget");
	widgets.back().setFont(this->app->font, 20);
	widgets.back().setText(letter);
	widgets.back().setPosition(sf::Vector2f(x, y));
	widgets.back().setOnClick([this, letter](Stage*) {
	  json msg;
	  msg["type"] = USER_WORD;
	  msg["guess"] = letter;
	  msg["round"] = app->getRound();
	  
	  app->sendMessage(msg);
	});
      };
    for (int i=0; i<10; ++i) {
      std::string letter = std::string(1, static_cast<char>(i+'A'));
      mkLetterWidget(letter, 100+i*55, 690);
    }
    for (int i=10; i<19; ++i) {
      std::string letter = std::string(1, static_cast<char>(i+'A'));
      mkLetterWidget(letter, 125+(i-10)*55, 745);
    }
    for (int i=19; i<26; ++i) {
      std::string letter = std::string(1, static_cast<char>(i+'A'));
      mkLetterWidget(letter, 150+(i-19)*55, 800);
    }

    widgets.emplace_back(this, this->window, 100, 100);
    widgets.back().setId("viewer-info");
    widgets.back().disableRect = true;
    widgets.back().setFont(this->app->font, 60);
    widgets.back().setText("Spectating");
    centerElement(widgets.back(), 800);
    widgets.back().hidden = true;
    return;
  }
  if (stageIndex == STAGE_SCORES) {
    // Title
    widgets.emplace_back(this, this->window, 100, 100);
    widgets.back().disableRect = true;
    widgets.back().setFont(this->app->font, 70);
    widgets.back().setText("Game Over");
    centerElement(widgets.back(), 15);

    // Ranking header
    widgets.emplace_back(this, this->window, 100, 100);
    widgets.back().setId("ranking-header");
    widgets.back().disableRect = true;
    widgets.back().setFont(this->app->font, 50);
    widgets.back().setText("Top players");
    centerElement(widgets.back(), 135);

    // Ranking
    widgets.emplace_back(this, this->window, 100, 100);
    widgets.back().setId("ranking");
    widgets.back().disableRect = true;
    widgets.back().setFont(this->app->font, 30);
    widgets.back().textCenteredY = false;
    widgets.back().setText("");
    centerElement(widgets.back(), 225);

     // Ranking
    widgets.emplace_back(this, this->window, 100, 100);
    widgets.back().setId("scores-status");
    widgets.back().disableRect = true;
    widgets.back().setFont(this->app->font, 30);
    widgets.back().setText("");
    centerElement(widgets.back(), 700);
    
    return;    
  }
}

TextField& Stage::getTextField(std::string fieldName) {
  for (uint i=0; i<this->textFields.size(); ++i) {
    if (this->textFields.at(i).name == fieldName)
      return this->textFields.at(i);
  }
  std::string err = "Attempt to get a non-existent text field ("
    + fieldName + ")";
  error(1, 0, err.c_str());
  throw;
}

Widget& Stage::getWidget(std::string widgetName) {
  for (uint i=0; i<this->widgets.size(); ++i) {
    if (this->widgets.at(i).name == widgetName)
      return this->widgets.at(i);
  }
  std::string err = "Attempt to get a non-existent widget ("
    + widgetName + ")";
  error(1, 0, err.c_str());
  throw;
}

/**
   Handle a mouse hover
*/
void Stage::mouseMove(int x, int y) {
  for (uint i=0; i<this->widgets.size(); ++i) {
    this->widgets.at(i).manageMouseMove(x, y);
  }
}

/**
   Handle a left mouse click on given coordinates.
*/
void Stage::leftClick(int x, int y) {
  for (uint i=0; i<this->widgets.size(); ++i) {
    this->widgets.at(i).manageLeftClick(x, y);
  }

  for (uint i=0; i<this->textFields.size(); ++i) {
    this->textFields.at(i).manageLeftClick(x, y);
  }
}

/**
   Handle a key press.
*/
void Stage::keyPress(int key) {
  for (uint i=0; i<this->textFields.size(); ++i) {
    this->textFields.at(i).manageKeyPress(key);
  }
}
  
void Stage::manageEvent(sf::Event event) {
  if (this->eventHook) {
    if (!this->eventHook(event)) return;
  }
  if (event.type == sf::Event::MouseButtonPressed &&
      event.mouseButton.button == sf::Mouse::Button::Left) {
    this->leftClick(event.mouseButton.x, event.mouseButton.y);
    return;
  }
  if (event.type == sf::Event::MouseMoved) {
    this->mouseMove(event.mouseMove.x, event.mouseMove.y);
    return;
  }
  if (event.type == sf::Event::KeyPressed) {
    this->keyPress(event.key.code);
  }
}

void Stage::render() {
  for (uint i=0; i<this->widgets.size(); ++i) {
    this->widgets.at(i).render();
  }
  for (uint i=0; i<this->texts.size(); ++i) {
    this->window->draw(this->texts.at(i));
  }
  for (uint i=0; i<this->textFields.size(); ++i) {
    this->textFields.at(i).render();
  }
  for (uint i=0; i<this->sprites.size(); ++i) {
    if (this->showSprites.at(i))
      this->window->draw(this->sprites.at(i));
 }
}

void Stage::addWidget(Widget btn) {
  this->widgets.push_back(btn);
}

Stage::Stage(App* appP, std::shared_ptr<sf::RenderWindow> win) {
  this->app = appP;
  this->window = win;
  this->stageIndex = -1;
}

Stage::~Stage() {

}
