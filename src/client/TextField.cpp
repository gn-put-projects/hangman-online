#include "TextField.h"

void TextField::setFocus(bool f) {
  if (f && this->neverFocused && this->eraseOnFirstFocus) {
    this->setText("");
  }
  this->focused = f;
  if (this->focused) {
    if (this->addCharCallback)
      this->addCharCallback(this, '\0');
    this->setColor(FOCUSED_COLOR);
    this->neverFocused = false;
  }
  else this->setColor(UNFOCUSED_COLOR);
}

void TextField::reset() {
  setFocus(false);
  this->neverFocused = true;
}

void TextField::manageLeftClick(int mx, int my) {
  bool focused =  (mx > this->x && mx < this->x+this->width &&
		   my > this->y && my < this->y+this->height);
  setFocus(focused);
}

/**
   Sets the addCharCallback lambda. The char argument of the lambda
   is the currently added char. If a backspace is pressed, the text
   is modified and the callback is called with '\0' as this argument.
   When the TextField is focused this lambda is run with '\0' as the
   argument as well.
*/
void TextField::setOnInput(std::function<void(TextField*, char)> cb) {
  this->addCharCallback = std::move(cb);
}

/**
   Add a char to the TextField's text.
   Do nothing if given char was '\0'.
*/
void TextField::defaultAddChar(char c) {
  if (c == '\0') return;
  if (this->text.size() < this->maxTextLength) {
    this->setText(this->text + c);
  }
}

/**
  Add a char to the text field
*/
void TextField::addChar(char c) {
  if (this->addCharCallback) {
    this->addCharCallback(this, c);
    return;
  }
  this->defaultAddChar(c);
}

void TextField::manageKeyPress(int key) {
  if (!this->focused) return;
  if (key == sf::Keyboard::Backspace) {
    this->setText(this->text.substr(0, this->text.size()-1));
    if (this->addCharCallback) {
      this->addCharCallback(this, '\0');
      return;
    }
    return;
  }
  if (key == sf::Keyboard::Escape) {
    setFocus(false);
    return;
  }
  if (key == sf::Keyboard::Space) {
    if (this->spaceEnabled)
      addChar(' ');
    return;
  }
  if (key >= sf::Keyboard::A && key <= sf::Keyboard::Z
      && this->alphaEnabled) {
    char c = key + 'a';
    addChar(c);
    return;
  }
  if (key >= sf::Keyboard::Num0 && key <= sf::Keyboard::Num9
      && this->numbersEnabled) {
    char c = key-sf::Keyboard::Num0 + '0';
    addChar(c);
    return;
  }
  if (key == sf::Keyboard::Period && this->numbersEnabled) {
    addChar('.');
    return;
  }
}

TextField::TextField(Stage* s,
		     std::string n,
		     std::shared_ptr<sf::RenderWindow> win,
		     int w, int h)
  : Widget(s, win, w, h) {
  this->name = n;
  this->focused = false;
  this->alphaEnabled = true;
  this->numbersEnabled = true;
  this->spaceEnabled = true;
  this->maxTextLength = 100;
  this->textCentered = false;
  this->neverFocused = true;
  this->eraseOnFirstFocus = false;
  setFocus(false);
}

TextField::~TextField() {

}
