#ifndef GAMEUPDATER_H
#define GAMEUPDATER_H

#include <iostream>
#include <map>
#include <string>
#include <ctype.h>      /* tolower */
#include <vector>       /* vectors */
#include "../shared/messageCodes.h"
#include <nlohmann/json.hpp>    /*json*/
using json = nlohmann::json;

class GameUpdater;

#define ROLE_VOID 0
#define ROLE_VIEWER 1
#define ROLE_PLAYER 2

#include "App.h"

/**
   Class for parsing the JSON content received form the server
   and updating the App's state accordingly.
*/
class GameUpdater {
private:
  std::string coveredWord;
  std::string notification;
  std::string lastGuessedWord;
  std::string lobbyStatus;
  std::string scoresStatus;
  
  // Player info
  int score;
  int time;
  int gallowParts;
  bool tried_letters[26];
  std::vector<std::string> used_words;
  bool isViewer;
  std::string nick;
  App* app;
    
public:
  std::string nickname;  
  int round;
  GameUpdater(App*);
    
  //scores user's guess (in a positive or negative way)
  //int usersGuess(std::string guess, int round);
  //reads JSON
  void readJson(json j1);
};

#endif
