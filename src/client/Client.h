#ifndef CLIENT_H
#define CLIENT_H

#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <error.h>
#include <netdb.h>
#include <sys/epoll.h>
#include <poll.h> 
#include <thread>
#include <vector>
#include "../shared/net.h"
#include <memory>
#include "../shared/ReadWriter.h"

class Client;

#include "App.h"

class Client {
private:
  int sock;
  int eFd;
  std::unique_ptr<ReadWriter> readWriter;
  App* app;
public:
  void sendMessage(std::string);
  Client(App*);
  std::string errorMsg;
  void disconnect();
  bool connectToServer(std::string);
  void update();
  ~Client();
};

#endif
