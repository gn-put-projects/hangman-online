#ifndef TEXTFIELD_H
#define TEXTFIELD_H

#include <iostream>
#include <SFML/Graphics.hpp>
#include <memory>
#include <functional>
#include <error.h>

#include "Widget.h"

#define FOCUSED_COLOR sf::Color(90, 90, 170)
#define UNFOCUSED_COLOR WIDGET_COLOR

class TextField : private Widget {
private:
  std::function<void(TextField*, char)> addCharCallback;
  bool neverFocused;
public:
  bool eraseOnFirstFocus;
  std::string name;
  void setOnInput(std::function<void(TextField*, char)>);
  bool focused;
  void manageKeyPress(int);
  void defaultAddChar(char);
  void addChar(char);
  void setFocus(bool);
  void reset();
  bool alphaEnabled;
  bool numbersEnabled;
  bool spaceEnabled;
  TextField(Stage*, std::string, std::shared_ptr<sf::RenderWindow>, int, int);
  void manageLeftClick(int, int) override;
  uint maxTextLength;
  using Widget::text;
  using Widget::setFont;
  using Widget::setText;
  using Widget::render;
  using Widget::getLocalBounds;
  using Widget::setPosition;
  using Widget::disableRect;
  using Widget::hidden;
  using Widget::textPaddingX;
  using Widget::textPaddingY;
  ~TextField();
};

#endif
