#include "App.h"

/**
   Pop a stage index from the top of the stage stack
   if there is more than one element on the stack.
*/
void App::popStage() {
  if (this->stageStack.size() > 1)
    this->stageStack.pop();
}

void App::goToBottomStage() {
  while (this->stageStack.size() > 1)
    this->stageStack.pop();
}

/**
   Resets the current stage by calling .reset()
*/
void App::resetCurrentStage() {
  this->topStage()->resetStage();
}

/**
   Push a specified stage index to the stage stack
*/
void App::pushStage(int stageIndex) {
  this->stageStack.push(stageIndex);
  this->resetCurrentStage();
}

/**
   Show a screen with custom text and an OK widget
*/
void App::popUp(std::string message) {
  if (this->currentStage() == STAGE_POPUP)
    this->popStage();
  this->pushStage(STAGE_POPUP);
  this->topStage()->getWidget("message").setText(message);
}

/**
   Get the currently displayed stage's index.
   -1 is returned on empty stage stack.
*/
int App::currentStage() {
  if (this->stageStack.empty())
    return -1;
  return this->stageStack.top();
}

int App::getRound() {
  return this->gameUpdater->round;
}

/**
   Hides all images on the current stage
   except one with the specified index.
*/
void App::showSingleImage(int index) {
  this->topStage()->hideAllImages();
  this->topStage()->setImageVisibility(index, true);
}

/**
   Access the currently displayed stage
*/
std::unique_ptr<Stage>& App::topStage() {
  if (this->stageStack.empty())
    error(1, 0, "Attempt to get the top stage, but the stage stack is empty");
  switch(this->stageStack.top()) {
  case STAGE_MENU:
    return this->mainMenu;
  case STAGE_CONNECT:
    return this->connectMenu;
  case STAGE_LOBBY:
    return this->lobbyStage;
  case STAGE_POPUP:
    return this->popupStage;
  case STAGE_GAME:
    return this->gameStage;
  case STAGE_SCORES:
    return this->scoresStage;
  }
  error(1, 0, "Incorrect top stage!");
  throw;
}

/**
   Process a received message
*/ 
void App::processMessage(std::string msg) {
  if (msg == "") return;
  this->gameUpdater->readJson(json::parse(msg));
}

/**
   Send a json message to the server
*/
void App::sendMessage(json j) {
  this->client->sendMessage(j.dump());
}

/**
   Display a waiting screen, connect to server
   at the specified IP and handle errors
*/
void App::connectToServer(std::string ipAddress,
			  std::string nick) {
  // Display the waiting screen
  this->popUp("Connecting...");
  this->render();
  
  // Wait for a bit to make sure the wait screen is
  // comfortably perceived (instant connection could
  // result in an unpleasant screen flash)
  std::this_thread::sleep_for(std::chrono::milliseconds(300));
  this->clearEvents();
  
  this->gameUpdater->nickname = nick;
  
  // Handle connecting and potential errors
  client->disconnect();
  bool connected = client->connectToServer(ipAddress);
  this->clearEvents();
  if (!connected) {
    this->popUp("Connecting to address "
		+ ipAddress + " failed.\n" + client->errorMsg + ".");
    return;
  }
}

/**
   Get value of a text field on the current stage.
*/
std::string App::getInput(std::string textFieldName) {
  return topStage()->getTextField(textFieldName).text;
}

/**
   Get value of a widget on the current stage.
*/
std::string App::getWidgetInput(std::string widgetName) {
  return topStage()->getWidget(widgetName).text;
}

/**
   Get a reference to a widget on the current stage.
*/
Widget& App::getWidget(std::string widgetName) {
  return topStage()->getWidget(widgetName);
}

/**
   Get a reference to a text field on the current stage.
*/
TextField& App::getTextField(std::string name) {
  return topStage()->getTextField(name);
}

/**
   Create all the accessible stages of the application
*/
void App::createStages() {
  this->mainMenu = std::make_unique<Stage>(this, this->window);
  this->mainMenu->setupStage(STAGE_MENU);
  
  this->connectMenu = std::make_unique<Stage>(this, this->window);
  this->connectMenu->setupStage(STAGE_CONNECT);

  this->lobbyStage = std::make_unique<Stage>(this, this->window);
  this->lobbyStage->setupStage(STAGE_LOBBY);

  this->popupStage = std::make_unique<Stage>(this, this->window);
  this->popupStage->setupStage(STAGE_POPUP);

  this->gameStage = std::make_unique<Stage>(this, this->window);
  this->gameStage->setupStage(STAGE_GAME);

  this->scoresStage = std::make_unique<Stage>(this, this->window);
  this->scoresStage->setupStage(STAGE_SCORES);
}

/**
   Sets up all variables needed for SFML display.
   Throws errors with loading fonts.
*/
void App::setupSFML() {
  this->windowName = "Hangman Online";
  this->windowSize.width = 1600;
  this->windowSize.height = 900;
  
  //this->config TODO
}

/**
   Creates the application's window
*/
void App::initSFML() {
  this->window = std::make_shared<sf::RenderWindow>(this->windowSize, this->windowName);
  this->window->setFramerateLimit(30);

  this->font = std::make_shared<sf::Font>();
  
  if (!this->font->loadFromFile
      ("res/fonts/RobotoMono/RobotoMono-Medium.ttf")) {
    error(1, 0, "Font arial.ttf could not be loaded");
  }
}

/**
   Clears SFML events except sf::Event::Closed.
   To be used after displaying a screen where
   input shall be disregarded.
*/
void App::clearEvents(){
  while (this->window->pollEvent(this->event)) {
    if (this->event.type == sf::Event::Closed) {
      this->window->close();
    }
  }
}

/**
   Manages SFML events using the current stage.
*/
void App::manageEvents(){
  while (this->window->pollEvent(this->event)) {
    if (this->event.type == sf::Event::Closed) {
      this->window->close();
    }
    
    topStage()->manageEvent(this->event);
  }
}

/**
   Updates the stage of the application
*/
void App::update(){
  this->manageEvents();
  this->client->update();
}

void App::render(){
  if (!this->window->isOpen()) return;
  this->window->clear(sf::Color(0, 0, 20));
  topStage()->render();
  
  this->window->display();
}

void App::loop() {
  while(this->window->isOpen()) {
    this->update();
    //if (this->window->isOpen()) return;
    this->render();
    //auto [x,y] = sf::Mouse::getPosition(*this->window);
    //std::cout<<"Mouse Position: " << x << " " << y << "\r";
  }
}

App::App() {
  this->setupSFML();
  this->initSFML();

  this->client = std::make_unique<Client>(this);
  this->gameUpdater = std::make_unique<GameUpdater>(this);

  this->createStages();

  this->pushStage(STAGE_MENU);
}

App::~App() {

}
