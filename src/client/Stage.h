#ifndef STAGE_H
#define STAGE_H

#include <iostream>
#include <SFML/Graphics.hpp>
#include <memory>

#define STAGE_MENU 0
#define STAGE_CONNECT 1
#define STAGE_LOBBY 2
#define STAGE_POPUP 3
#define STAGE_GAME 4
#define STAGE_SCORES 5

class Stage;

#include "App.h"
#include "Widget.h"
#include "TextField.h"

#define ROLE_VOID 0
#define ROLE_VIEWER 1
#define ROLE_PLAYER 2

/**
   An object representing one specific stage in the application.
 */
class Stage {
private:
  std::shared_ptr<sf::RenderWindow> window;
  std::vector<Widget> widgets;
  std::vector<TextField> textFields;
  std::vector<sf::Text> texts;
  void leftClick(int, int);
  void mouseMove(int x, int y);
  void keyPress(int);
  App* app;
  template <class T>
  void centerElement(T&, float);
  /**
     Hook run on every manageEvent before objects in the stage
     can manage the given event. If false is returned other objects'
     manageEvent function are not run.
  */
  std::function<bool(sf::Event)> eventHook;

  std::vector<std::unique_ptr<sf::Texture> > textures;
  std::vector<bool> showSprites;
  std::vector<sf::Sprite> sprites;
public:
  int stageIndex;
  void addImage(std::string);
  sf::Sprite& getImage(int);
  void hideAllImages();
  void setImageVisibility(int, bool);
  void setEventHook(std::function<bool(sf::Event)>);
  TextField& getTextField(std::string);
  Widget& getWidget(std::string);
  void manageEvent(sf::Event);
  void render();
  void addWidget(Widget);
  void setupStage(int);
  void resetStage();
  Stage(App*, std::shared_ptr<sf::RenderWindow>);
  ~Stage();
};

#endif
