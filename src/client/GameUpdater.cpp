#include "GameUpdater.h"

GameUpdater::GameUpdater(App* appP){
  this->app = appP;
  this->round = 0;
  for(uint i=0; i<std::size(this->tried_letters); i++){
    this->tried_letters[i] = false;
  }
  this->nickname = "";
  this->lastGuessedWord = "";
  this->notification = "";
  this->lobbyStatus = "";
  this->scoresStatus = "";
}

void GameUpdater::readJson(json j){
  int t = j["type"];
  //std::cout<<"Client received: "<<messageType(t)<<"\n";
  
  switch(t){
  case SERVER_KICK: {
    app->client->disconnect();
    std::string reason = j["reason"];
    app->goToBottomStage();
    app->popUp("Kicked from server.\nReason: " + reason);
    break;
  }
  case SERVER_USER_ACCEPTED: {
    json msg;
    msg["type"] = USER_SEND_NICKNAME;
    msg["nickname"] = this->nickname;

    app->sendMessage(msg);
    break;
  }

  case SERVER_NICKNAME_USED: {
    bool nicknameOk = j["ok"];
    if (!nicknameOk) {
      app->popUp("The specified nickname is already used");
      return;
    }
    break;
  }
  case SERVER_UPDATE_LOBBY: {
    if (app->currentStage() != STAGE_LOBBY)
      app->pushStage(STAGE_LOBBY);
    
    this->lobbyStatus = j["status"];

    app->getWidget("lobby-players")
      .setText(j["playerList"]);
    app->getWidget("lobby-state").setText(this->lobbyStatus);

    Widget& playerWidget = app->getWidget("player-widget");
    Widget& viewerWidget = app->getWidget("viewer-widget");

    int role = j["role"];
    if (role == ROLE_PLAYER) {
      playerWidget.setText("X");
      viewerWidget.setText("");
      app->getWidget("ready-widget").hidden = false;
    }
    else if (role == ROLE_VIEWER) {
      viewerWidget.setText("X");
      playerWidget.setText("");
      app->getWidget("ready-widget").hidden = true;
    }
    else {
      playerWidget.setText("");
      viewerWidget.setText("");
      app->getWidget("ready-widget").hidden = true;
    }

    int ready = j["ready"];
    app->getWidget("ready-widget")
      .setText((ready? "CANCEL" : "READY"));
    
    break;
  }
  case SERVER_UPDATE_GAME:{
    if (app->currentStage() != STAGE_GAME)
      app->pushStage(STAGE_GAME);
    
    this->coveredWord = j["coveredWord"];
    this->isViewer = j["isViewer"];
    for (uint i = 0; i<std::size(this->tried_letters); i++) {
      this->tried_letters[i] = j["triedLetters"][i];
      std::string letter = std::string(1, static_cast<char>(i+'A'));
      Widget& letterWidget = app->getWidget(letter + "-widget");
      if (this->isViewer) {
	letterWidget.hidden = true;
      }
      else if (this->tried_letters[i] && !letterWidget.isDisabled()) {
	letterWidget.disable();
      }
      else if (!this->tried_letters[i] && letterWidget.isDisabled()) {
	letterWidget.enable();
      }
    }
    this->lastGuessedWord = j["lastGuessedWord"];
    this->time = j["time"];
    this->score = j["score"];
    this->gallowParts = j["gallowParts"];
    if (j["round"] != this->round) {
      app->resetCurrentStage();
      this->round = j["round"];
    }
    this->notification = j["notification"];
    std::string ranking = j["ranking"];

    if (this->isViewer) {
      app->getWidget("viewer-info").hidden = false;
      app->getTextField("guess-text").hidden = true;
      app->getWidget("guess-widget").hidden = true;
      app->getWidget("time-text").hidden = true;
    }
    else {
      app->getWidget("time-text")
	.setText(std::to_string(this->time)+std::string(1, 's'));
    }
    app->getWidget("lastguess")
      .setText("Last word: " + this->lastGuessedWord);
    
    app->getWidget("score-text")
      .setText("Score: " + std::to_string(this->score));

    app->getWidget("covered-word")
      .setText(this->coveredWord);

    app->getWidget("ranking")
      .setText(ranking);

    app->getWidget("notification")
      .setText(this->notification);

    app->showSingleImage(this->gallowParts);
      
    break;
  }
  case SERVER_UPDATE_SCORES:{
    if (app->currentStage() != STAGE_SCORES)
      app->pushStage(STAGE_SCORES);
    
    this->scoresStatus = j["status"];
    app->getWidget("scores-status").setText(this->scoresStatus);

    app->getWidget("ranking").setText(j["ranking"]);
  }
  }
    
}
