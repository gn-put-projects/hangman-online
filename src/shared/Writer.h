#ifndef WRITER_H
#define WRITER_H

#include <iostream>
#include <queue>
#include "net.h"

class Writer;

class Writer {
private:
  int fd;
  int eFd;
  std::queue<std::string> writeBuffer;
public:
  Writer(int, int);
  void requestWrite(std::string);
  void writeNow();
  void setWriteRequest(bool);
  ~Writer();
};

#endif
