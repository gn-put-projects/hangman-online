#include "ReadWriter.h"

void ReadWriter::requestWrite(std::string message) {
  this->writer->requestWrite(message + this->stopSeq);
}

void ReadWriter::writeNow() {
  this->writer->writeNow();
}

std::string ReadWriter::receiveMessage() {
  return this->reader->receiveMessage();
}

bool ReadWriter::readNow() {
  return this->reader->readNow();
}

ReadWriter::ReadWriter(int fd, int eFd, std::string stopSequence) {
  this->stopSeq = stopSequence;
  this->reader = std::make_unique<Reader>(fd, eFd, this->stopSeq);
  this->writer = std::make_unique<Writer>(fd, eFd);
}

ReadWriter::~ReadWriter() {
}
