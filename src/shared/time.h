#ifndef TIME_H
#define TIME_H

#include <iostream>
#include <string>
#include <chrono>

namespace gntime {
  /**
     Get time since epoch in milliseconds
  */
  long long now();
}

#endif
