#include "Writer.h"

Writer::Writer(int fileDescriptor, int epollFd) {
  this->fd = fileDescriptor;
  this->eFd = epollFd;
}

void Writer::requestWrite(std::string text) {
  this->writeBuffer.push(text);
  if (this->writeBuffer.size() == 1) {
    this->setWriteRequest(true);
  }
}

/*
  Write requested contents. To be called on EPOLLOUT event
*/
void Writer::writeNow() {
  std::string msg = this->writeBuffer.front();
  const char* buffer = msg.c_str();
  int bufferSize = msg.length();

  int wrote = write(this->fd, buffer, bufferSize);

  if (wrote == -1) {
    if (errno | (EAGAIN | EWOULDBLOCK)) {
      //std::cout<<"Write not ready!\n";
      return;
    }
    error(1, errno, "Write");
  }

  if (wrote < bufferSize) {
    //std::cout<<"[WRITER]: "<<msg.substr(0,wrote)<<"\n";
    //std::cout<<"Uncomplete message wrote: "<<wrote<<"/"<<bufferSize<<"\n";
    //std::cout<<"Sent: "<<msg.substr(0,wrote)<<". Left to send: "
    //	     <<msg.substr(wrote)<<"\n";
    this->writeBuffer.front() = msg.substr(wrote);
    return;
  }
  //std::cout<<"[WRITER]: "<<msg<<"\n";
  //std::cout<<"Whole message sent: " << msg << "\n";
  this->writeBuffer.pop();
  if (this->writeBuffer.size() == 0) {
    this->setWriteRequest(false);
  }
}

void Writer::setWriteRequest(bool await) {
  epoll_event e;
  e.events = (await)? (EPOLLIN | EPOLLOUT) : EPOLLIN;
  e.data.fd = this->fd;

  epoll_ctl(this->eFd, EPOLL_CTL_MOD, this->fd, &e);
}

Writer::~Writer() {
  this->setWriteRequest(false);
}
