#ifndef MESSAGECODES_H
#define MESSAGECODES_H

#include <string>
/**
  Get the name of a messageType from its code.
*/
std::string messageType(int);

enum MessageCodes {
  /**
     User's reply to SERVER_USER_ACCEPTED.
     @field <std::string> nickname
  */
  USER_SEND_NICKNAME=700,
  /**
     User's role declaration.
     @field <int> role (ROLE_PLAYER or ROLE_VIEWER)
  */
  USER_ROLE_DECLARED,
  /**
     User's ready / not ready declaration.
     @field <bool> ready
  */
  USER_READY_CHANGE,
  /**
     User's letter or whole word guess
     @field <std::string> guess
     @field <int> round
  */
  USER_WORD,
  /**
     Server's message issued to kick a user
     @field <std::string> reason
  */
  SERVER_KICK,
  /**
     Server's lobby update message
     @field <std::string> status
     @field <std::string> playerList
  */
  SERVER_UPDATE_LOBBY,
  /**
     Server's game update message
     @field <std::string> coveredWord
     @field <bool[26]> triedLetters
     @field <std::string> lastGuessedWord
     @field <int> round
     @field <std::string> ranking
     @field <std::string> notification
     
     Player info:
     
     @field <int> time
     @field <int> score
     @field <int> gallowParts
     @field <int> role
  */
  SERVER_UPDATE_GAME,
  /**
     Server's greeting after accepting a new connection.
     No fields except `type`.
  */
  SERVER_USER_ACCEPTED,
  /**
    Server's reply to USER_SEND_NICKNAME if given
    nickname is already used.
    @field <bool> ok (always false)
  */
  SERVER_NICKNAME_USED,
  /**
     Server's score screen update message
     @field <std::string> ranking
     @field <std::string> status
  */
  SERVER_UPDATE_SCORES
};

#endif
