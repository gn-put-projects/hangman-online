#include "net.h"

namespace net {
  uint readData(int fd, char* buffer, uint bufferSize){
    int readBytes = read(fd, buffer, bufferSize);
    if (readBytes == -1)
      error(1, errno, "Read failed (%d)", fd);
    return (uint) readBytes;
  }
  
  void writeData(int fd, const char* buffer, uint bufferSize){
    int wrote = write(fd, buffer, bufferSize);
    if (wrote == -1)
      error(1, errno, "Write failed (%d)", fd);
    if ((uint) wrote != bufferSize) {
      error(0, errno, "Wrote incomplete message (%d) [%d/%d]",
	    fd, bufferSize, wrote);
    }
  }

  void printEvents(uint mask) {
    uint printed = 0;
    std::cout<<"{";
    for (uint i=0; i<EPOLL_EVENTS_VECTOR.size(); ++i) {
      if (mask & EPOLL_EVENTS_VECTOR[i].second) {
	if (printed > 0) std::cout<<", ";
	std::cout << EPOLL_EVENTS_VECTOR[i].first;
	printed++;
      }
    }
    std::cout<<"}\n";
  }

}
