#ifndef NET_H
#define NET_H

#include <iostream>
#include <vector>
#include <utility>
#include <string>
#include <unistd.h>
#include <errno.h>
#include <error.h>
#include <sys/epoll.h>
#include <cstring>
#include <fcntl.h>
#include <unordered_set>

typedef unsigned int uint;
template<class T> using uset = std::unordered_set<T>;

namespace net {
  uint readData(int fd, char* buffer, uint bufferSize);
  void writeData(int fd, const char* buffer, uint bufferSize);

  const std::vector<std::pair<std::string, unsigned int>>
  EPOLL_EVENTS_VECTOR = {
    {"EPOLLIN", EPOLLIN}, // 0x001
    {"EPOLLPRI", EPOLLPRI}, // 0x002
    {"EPOLLOUT", EPOLLOUT}, // 0x004
    {"EPOLLRDNORM", EPOLLRDNORM}, // 0x040
    {"EPOLLRDBAND", EPOLLRDBAND}, // 0x080
    {"EPOLLWRNORM", EPOLLWRNORM}, // 0x100
    {"EPOLLWRBAND", EPOLLWRBAND}, // 0x200
    {"EPOLLMSG", EPOLLMSG}, // 0x400
    {"EPOLLERR", EPOLLERR}, // 0x008
    {"EPOLLHUP", EPOLLHUP}, // 0x010
    {"EPOLLRDHUP", EPOLLRDHUP}, // 0x2000
    {"EPOLLONESHOT", EPOLLONESHOT}, // (1 << 30)
    {"EPOLLET", EPOLLET} // (1 << 31)
  };
    
  void printEvents(uint mask);
}

#endif
