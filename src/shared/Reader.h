#ifndef READER_H
#define READER_H

#include <iostream>
#include <stack>

#include "net.h"

#define BUFSIZE 256

class Reader;

class Reader {
private:
  int fd;
  int eFd;
  /**
     The currently read message.
  */
  char buf[BUFSIZE];
  std::string readBuffer;
  std::string stopSeq;
  std::stack<std::string> messages; 
public:
  Reader(int, int, std::string="STOP");
  std::string receiveMessage();
  void analyzeReadBuffer();
  void setStopSeq(std::string);
  bool readNow();
  ~Reader();
};

#endif
