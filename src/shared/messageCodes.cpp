#include "messageCodes.h"

std::string messageType(int code) {
  switch(code) {
  case USER_SEND_NICKNAME:
    return "USER_SEND_NICKNAME";
  case USER_ROLE_DECLARED:
    return "USER_ROLE_DECLARED";
  case USER_READY_CHANGE:
    return "USER_READY_CHANGE";
  case USER_WORD:
    return "USER_WORD";
  case SERVER_KICK:
    return "SERVER_KICK";
  case SERVER_UPDATE_LOBBY:
    return "SERVER_UPDATE_LOBBY";
  case SERVER_UPDATE_GAME:
    return "SERVER_UPDATE_GAME";
  case SERVER_USER_ACCEPTED:
    return "SERVER_USER_ACCEPTED";
  case SERVER_NICKNAME_USED:
    return "SERVER_NICKNAME_USED";
  }
  return "UNKNOWN_CODE";
}
