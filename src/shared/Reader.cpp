#include "Reader.h"

Reader::Reader(int fileDescriptor, int epollFd, std::string stop) {
  this->fd = fileDescriptor;
  this->eFd = epollFd;
  this->readBuffer = "";
  this->stopSeq = stop;
}

/**
   Check if any full messages are available.
   If so, return the first one. Return an empty string
   otherwise.
*/
std::string Reader::receiveMessage() {
  if (this->messages.empty()) {
    //std::cout<<"Reader: No full messages are ready\n";
    return "";
  }
  
  std::string msg = this->messages.top();
  //std::cout<<"Reader: Read full message: "<<msg<<"\n";
  this->messages.pop();

  return msg;
}

/**
  Read available contents. To be called on EPOLLIN event.
  @return True on successful read, false if 0 bytes were read.
*/
bool Reader::readNow() {
  uint read = net::readData(this->fd, this->buf, BUFSIZE);
  if (read == 0) return false;
  
  std::string readNow = std::string(this->buf).substr(0, read);
  //std::cout<<"readNow read: "<<readNow<<"\n";
  
  this->readBuffer += readNow;
  
  this->analyzeReadBuffer();
  
  return true;
}

/**
   Check if the read buffer contains full messages.
*/
void Reader::analyzeReadBuffer() {
  while (true) {
    size_t found = this->readBuffer.find(this->stopSeq);
    if (found == std::string::npos) return;

    std::string msg = this->readBuffer.substr(0, found);
    this->readBuffer = this->readBuffer.substr(found + this->stopSeq.size());
    this->messages.push(msg);
  }
}

void Reader::setStopSeq(std::string newStopSeq) {
  this->stopSeq = newStopSeq;
}

Reader::~Reader() {
}
