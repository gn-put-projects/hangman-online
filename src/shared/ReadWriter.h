#ifndef READWRITER_H
#define READWRITER_H

#include <iostream>
#include "Reader.h"
#include "Writer.h"
#include <memory>

class ReadWriter;

class ReadWriter {
private:
  std::unique_ptr<Reader> reader;
  std::unique_ptr<Writer> writer;
  std::string stopSeq;
public:
  void requestWrite(std::string);
  void writeNow();
  std::string receiveMessage();
  bool readNow();
  ReadWriter(int, int, std::string="STOP");
  ~ReadWriter();
};

#endif
