#include "time.h"

namespace gntime {

  /**
     Return the number of milliseconds passed since the epoch
  */
  long long now() {
    return std::chrono::duration_cast<std::chrono::milliseconds>
      (std::chrono::system_clock::now().time_since_epoch())
      .count();
  }
 
}
