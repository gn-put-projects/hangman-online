#include "Game.h"

std::map<int, int> updateMessageTypes = {
  {STATE_LOBBY, SERVER_UPDATE_LOBBY},
  {STATE_GAME, SERVER_UPDATE_GAME},
  {STATE_SCORES, SERVER_UPDATE_SCORES}
};

Game::Game(Server* serverP){
  this->server = serverP;
  srand(std::time(NULL));
  this->reset(true);
}

/**
   Resets the state of the game to the initial state.
   Hard reset on `true` argument - also clears user related
   vectors. Otherwise users are reset to initial states.
*/
void Game::reset(bool hard) {
  this->round = 0;
  this->lastWord = "";
  this->word = "";
  this->covered_word = "";
  this->notification = "";
  for (uint i=0; i<std::size(this->tried_letters); i++) {
    this->tried_letters[i] = false;
  }
  used_words.clear();
  if (hard) {
    users.clear();
  }
  else {
    for (auto& [key, user] : users) {
      user.reset();
    }
  }
  this->time = gntime::now();
  this->lastUpdateTime = gntime::now();
  this->updateRequested = false;
  this->state = STATE_LOBBY;
  this->lobbyStatus="";
  this->lobbyTimer = LOBBY_START_TIME;
  this->lobbyCountdown = false;
  this->updateLobbyStatus(0);

  this->scoresStatus="";
  this->scoresTimer = SCORES_DISPLAY_TIME;
  this->scoresCountdown = false;
  this->gameStarted = false;
}

void Game::startGame() {
  this->gameStarted = true;
  this->round = -1;
  this->word = "";
  this->state = STATE_GAME;

  for (auto const& [key, user] : users) {
    if (user.role != ROLE_VOID) continue;
    json msg;
    msg["type"] = SERVER_KICK;
    msg["reason"] = "No role selected in lobby";
    server->sendMessage(key, msg);
  }
  this->nextRound();
}

/**
   Finish the current game and switch to the scores screen
*/
void Game::endGame() {
  this->gameStarted = false;
  this->state = STATE_SCORES;
}

void Game::createWord(){
  std::fstream file;
  file.open("res/data/words.dat", std::ios_base::in);

  int i = 1;
  int line = rand() % WORDS_IN_TXT;
  while(i != line){
    file.ignore(1000, '\n');
    ++i;
  }
  //std::string fContent;
  file >> this->word;
  this->covered_word = this->word;
  for(uint i=0; i<std::size(this->word); i++){
    this->covered_word[i] = COVERED_LETTER;
  }
  file.close();
    
}

/**
   Return the number of letters left to guess
*/
int Game::lettersLeft() {
  int dashes = 0;
  for(uint i=0; i < std::size(this->covered_word);i++){
    if(this->covered_word[i] == COVERED_LETTER) dashes++;
  }
  return dashes;
}

void Game::userFault(int id) {
  if (users.at(id).time < 0)
    users.at(id).time = 0;
  
  users.at(id).time += START_TIME;
  users.at(id).gallowParts += 1;
    
  if (users.at(id).gallowParts == PARTS_OF_GALLOWS)
    this->playerLost(id);

  if (this->playerCount() == 0) {
    this->endGame();
  }
}

void Game::letterGuess(int id, char letter) {
  int index = letter - 'a';
  if (this->tried_letters[index])
    return;

  this->tried_letters[index] = true;
  
  for (auto& [key, user] : users) {
    if(user.id == id) continue;
    user.time += BONUS_TIME;
  }
  
  if (!this->existsInString(letter)) {
    //this letter wasn't in the word
    this->notification =
      users.at(id).nickname +
      " tried letter " + std::string(1, letter) +
      " but it wasn't in the word";
    this->userFault(id);
    return;
  }
  
  this->updateCoveredString(letter);

  if(this->lettersLeft() == 0){
    // whole word guessed
    this->notification =
      users.at(id).nickname +
      " tried letter " + std::string(1, letter) +
      " and it completed the word!";
    
    users.at(id).time += START_TIME;
    users.at(id).score += WORDS_POINTS;
    this->nextRound();
    return;
  }
  // letter guessed
  this->notification =
    users.at(id).nickname +
    " tried letter " + std::string(1, letter) +
    " and it is present in the word!";
  users.at(id).time += START_TIME;
  users.at(id).score += LETTER_POINTS;
}

void Game::usersGuess(int id, std::string guess) {
  if(std::size(guess) == 1) {
    this->letterGuess(id, guess[0]);
  }

  if (std::size(guess) <= 1)
    return;
  
  if (this->existsInUsed_words(guess))
    return;
    
  this->used_words.push_back(guess);
  
  for (auto& [key, user] : users) {
    if (user.id == id) continue;
    user.time += BONUS_TIME;
  }

  if (this->word != guess) {
    //
    this->notification =
      users.at(id).nickname +
      " tried word " + guess +
      " but it's incorrect";
    this->userFault(id);
    return;
  }
  this->notification =
    users.at(id).nickname +
    " tried word " + guess +
    ". Bullseye!";
  users.at(id).time += START_TIME;
  users.at(id).score += WORDS_POINTS;
  this->covered_word = guess;
  this->nextRound();
}

int Game::addUser(int id, std::string nick){
  if (this->users.contains(id)) {
    return -1;
  }

  this->users.emplace(id, User(id, nick));
  return 1;
}

bool Game::existsInString(char symbol){
  for(uint i=0; i<std::size(this->word);i++){
    if(this->word[i] == symbol) return true;
  }
  return false;
}

void Game::updateCoveredString(char symbol){
  for(uint i=0; i<std::size(this->word);i++){
    if(this->word[i] == symbol)
      this->covered_word[i] = symbol;
  }
}

void Game::playerLost(int id) {
  users.at(id).role = ROLE_VIEWER;
  users.at(id).lost = true;
  this->updateRequested = true;
}

void Game::removeUser(int id) {
  users.erase(id);
  this->updateRequested = true;
}

bool Game::existsInUsed_words(std::string guess) {
  for(uint i = 0; i< std::size(this->used_words); i++){
    if(this->used_words[i] == guess) return true;
  }
  return false;
}

void Game::nextRound() {
  this->lastWord = this->word;
  this->round++;
  for(uint i=0; i<std::size(this->tried_letters); i++){
    this->tried_letters[i] = false;
  }
  this->used_words.clear();
  this->createWord();
}

/**
   Returns time to next planned update in milliseconds
*/
long long Game::timeToUpdate() {
  return UPDATE_PERIOD;
}

void Game::update() {
  long long now = gntime::now();

  long long passed = now - this->time;
  for (auto& [key, user] : users) {
    if (!this->gameStarted) continue;
    if (user.role != ROLE_PLAYER) continue;

    long long newTime = user.time - passed;
    if (user.time/1000 != newTime/1000) {
      this->updateRequested = true;
    }
    user.time = newTime;
    if (user.time <= 0) {
      this->userFault(user.id);
    }
  }
  if (this->state == STATE_LOBBY)
    this->updateLobbyStatus(passed);
  else if (this->state == STATE_SCORES)
    this->updateScoresStatus(passed);
  if (this->updateRequested) {
    this->updatePlayers();
  }
  this->time = now;
}

bool Game::playerLeft(){
  for (auto const& [key, user] : users) 
    if (user.role == ROLE_PLAYER) return true;
        
  return false;
}

/**
   Process a message received from the client
   and update the game's state accordingly.
*/
void Game::readJson(int id, json j) {
  this->processMessage(id, j);
  this->update();
}

void Game::processMessage(int id, json j) {
  int type = j["type"];
  //std::cout<<"Server received: "<<messageType(type)<<"\n";
  
  if (type == USER_SEND_NICKNAME) {
    std::string nick = j["nickname"];
    if (this->isNicknameUsed(nick)) {
      json msg;
      msg["type"] = SERVER_NICKNAME_USED;
      msg["ok"] = false;
      server->sendMessage(id, msg);
      return;
    }
    
    addUser(id, nick);
    
    if (this->gameStarted)
      users.at(id).role = ROLE_VIEWER;
            
    this->updateRequested = true;
    return;
  }
  if (type == USER_READY_CHANGE) {
    if (this->gameStarted) {
      return;
    }
    bool ready = j["ready"];
    users.at(id).ready = ready;
                
    this->updateRequested = true;
    return;
  }
  if (type == USER_ROLE_DECLARED) {
            
    if (this->gameStarted) {
      return;
    }
    
    int role = j["role"];
    users.at(id).role = role;
    users.at(id).ready = false;
        
    this->updateRequested = true;
    return;
  }
  if (type == USER_WORD) {
    if (users.at(id).role == ROLE_VIEWER) {
      std::cout<<"Viewer tried to guess a word!\n";
      return;
    }

    std::string guess = j["guess"];
    for(uint i = 0; i < std::size(guess);i++){
      guess[i] = std::tolower(guess[i]);
    }
    
    for(uint i = 0; i < std::size(guess);i++) {
      if(guess[i]<'a'||guess[i]>'z') {
	std::cout<<"Incorrect char: "<<guess[i]<<"\n";
	return;
      }
    }

    if(this->round != j["round"]) {
      std::cout<<"Player said it is round "<<j["round"]
	       <<" but it is round "<<this->round<<"\n";
      return;
    }
    usersGuess(id, guess);

    this->updateRequested = true;
    return;
  }
  error(1, 0, "Unrecognized message type received");
}

/**
   Send update messages to all players, according
   to the current game state.
*/
void Game::updatePlayers() {
  this->updateRequested = false;
  this->lastUpdateTime = gntime::now();
  
  json updateMessage;

  int messageType = updateMessageTypes.at(this->state);
  
  for (auto const& [key, user] : users) {
    updateMessage = createJson(key, messageType);
    server->sendMessage(key, updateMessage);
  }
 
}

/**
   Returns the number of users who were players
   but have already lost
*/
int Game::lostPlayerCount() {
  int c = 0;
  for (auto const& [key, user] : users) {
    if (user.lost) c++;
  }
  return c;
}

/**
   Returns the current number of players
*/
int Game::playerCount() {
  int c = 0;
  for (auto const& [key, user] : users) {
    if (user.role == ROLE_PLAYER) c++;
  }
  return c;
}

/**
   Returns the current number of players ready to
   start the game
*/
int Game::readyPlayerCount() {
  int c = 0;
  for (auto const& [key, user] : users) {
    if (user.role == ROLE_PLAYER &&
	user.ready)
      c++;
  }
  return c;
}

/**
   Returns the current number of viewers
*/
int Game::viewerCount() {
  int c = 0;
  for (auto const& [key, user] : users) {
    if (user.role == ROLE_VIEWER) c++;
  }
  return c;
}

/**
   Returns the current number of undecided users
*/
int Game::undecidedCount() {
  int c = 0;
  for (auto const& [key, user] : users) {
    if (user.role == ROLE_VOID) c++;
  }
  return c;
}

void Game::updateLobbyStatus(long long timePassed) {
  if (playerCount() < 2) {
    this->lobbyStatus = "Waiting for players...";
    this->lobbyCountdown = false;
    return;
  }
  if (readyPlayerCount() < 2) {
    this->lobbyStatus = "Waiting for players to ready up...";
    this->lobbyCountdown = false;
    return;
  }
  if (!this->lobbyCountdown) {
    // start a timer
    this->lobbyCountdown = true;
    this->lobbyTimer = LOBBY_START_TIME;
    this->lobbyStatus = "Starting in " +
      std::to_string(this->lobbyTimer/1000) + "s";
    this->updateRequested = true;
    return;
  }
  // at least 2 players and counting down
  this->lobbyTimer -= timePassed;
  if (this->lobbyTimer <= 0) {
    this->lobbyCountdown = false;
    this->lobbyStatus = "";
    this->startGame();
    this->updateRequested = true;
    return;
  }
  std::string newStatus =  "Starting in " +
    std::to_string(this->lobbyTimer/1000) + "s";
  if (this->lobbyStatus == newStatus) return;
  this->lobbyStatus = newStatus;
  this->updateRequested = true;
}

void Game::updateScoresStatus(long long timePassed) {
  if (!this->scoresCountdown) {
    // start a timer
    this->scoresCountdown = true;
    this->scoresTimer = SCORES_DISPLAY_TIME;
    this->scoresStatus = "Returning to lobby in " +
      std::to_string(this->scoresTimer/1000) + "s";
    this->updateRequested = true;
    return;
  }
  
  this->scoresTimer -= timePassed;
  if (this->scoresTimer <= 0) {
    this->scoresCountdown = false;
    this->scoresStatus = "";
    this->reset(false);
    this->updateRequested = true;
    return;
  }
  std::string newStatus = "Returning to lobby in " +
    std::to_string(this->scoresTimer/1000) + "s";
  if (this->scoresStatus == newStatus) return;
  this->scoresStatus = newStatus;
  this->updateRequested = true;
}

/**
   Prepare an update message for a specified user
*/
json Game::createJson(int id, int type){
  json j;
  if (type == SERVER_UPDATE_LOBBY) {
    j["type"] = type;
    j["status"] = this->lobbyStatus;
    j["role"] = users.at(id).role;
    j["playerList"] = this->getLobbyPlayerList();
    j["ready"] = users.at(id).ready;
    return j;
  }
  if (type == SERVER_UPDATE_SCORES) {
    j["type"] = type;
    j["status"] = this->scoresStatus;
    j["ranking"] = this->getScoresList(id);
    return j;
  }
  if (type == SERVER_UPDATE_GAME) {
    j["type"] = type;
    j["coveredWord"] = this->covered_word;
    for(uint i = 0; i<std::size(this->tried_letters); i++){
      j["triedLetters"][i] = this->tried_letters[i];
    }
    j["lastGuessedWord"] = this->lastWord;
    j["round"] = this->round;
    j["ranking"] = this->getTop3Ranking();
    j["notification"] = this->notification;
    j["isViewer"] = users.at(id).role == ROLE_VIEWER;
    
    j["time"] = users.at(id).time / 1000;
    j["score"] = users.at(id).score;
    j["gallowParts"] = users.at(id).gallowParts;
    return j;
  }
  error(1, 0, "Unrecognized message type passed to createJson");
  return j;
}

#define MAX_USERS_DISPLAYED 10

std::string Game::getLobbyPlayerList() {
  int p = 0;
  int v = 0;
  int u = 0;
  
  std::string pList = "Players:\n";
  std::string vList = "Viewers:\n";
  std::string uList = "Undecided:\n";
  for (auto const& [key, user] : users) {
    if (user.role == ROLE_VOID) {
      uList += std::to_string(++u) + ". " + user.nickname + "\n";
    }
    else if (user.role == ROLE_PLAYER) {
      pList += std::to_string(++p) + ". " + user.nickname +
	(user.ready ? " [READY]" : " [NOT READY]") + "\n";
    }
    else {
      vList += std::to_string(++v) + ". " + user.nickname + "\n";
    }
    if (p+v+u == MAX_USERS_DISPLAYED)
      break;
  }
  return (p ? pList : "") + (v ? vList : "") +
    (u ? uList : "");
}

#define MAX_SCORES_DISPLAYED 5

std::string Game::getScoresList(int playerId) {
  int p = 0;
  std::vector<int> sortedPlayers;
  sortedPlayers.clear();
  sortedPlayers.reserve(this->lostPlayerCount());
  
  for (auto const& [key, user] : users) {
    if (user.lost) {
      sortedPlayers.emplace_back(key);
    }
  }
  std::function<bool(int&, int&)> compareUsers =
    [this](int& a, int& b) -> bool {
      return users.at(a).score > users.at(b).score;
    };
  
  std::sort(sortedPlayers.begin(), sortedPlayers.end(), compareUsers);
  
  std::string output = "";
  
  p = 0;
  for (auto const& id : sortedPlayers) {
    if (p >= MAX_SCORES_DISPLAYED) break;
    output += std::to_string(++p) + ". " + users.at(id).nickname +
      ((id == playerId)? " (YOU) - " : " - ") +
      std::to_string(users.at(id).score) + "\n";
  }
  return output;
}

std::string Game::getTop3Ranking() {
  int id1=-1, id2=-1, id3=-1;
  int maxScore = -1;
  for (auto const& [key, user] : users) {
    if (user.role != ROLE_PLAYER && !user.lost) continue;
    if (user.score > maxScore) {
      maxScore = user.score;
      id1 = user.id;
    }
  }
  maxScore = -1;
  for (auto const& [key, user] : users) {
    if (user.role != ROLE_PLAYER && !user.lost) continue;
    if (user.id == id1) continue;
    if (user.score > maxScore) {
      maxScore = user.score;
      id2 = user.id;
    }
  }
  maxScore = -1;
  for (auto const& [key, user] : users) {
    if (user.role != ROLE_PLAYER && !user.lost) continue;
    if (user.id == id1 || user.id == id2) continue;
    if (user.score > maxScore) {
      maxScore = user.score;
      id3 = user.id;
    }
  }

  if (id1 == -1) return "";
  
  std::string ranking =
    "1. "+users.at(id1).nickname+" - "+
    std::to_string(users.at(id1).score)+"\n";

  if (id2 == -1) return ranking;
  ranking +=
    "2. "+users.at(id2).nickname+" - "+
    std::to_string(users.at(id2).score)+"\n";

  if (id3 == -1) return ranking;
  
  return ranking +
    "3. "+users.at(id3).nickname+" - "+
    std::to_string(users.at(id3).score)+"\n";
}

bool Game::isNicknameUsed(std::string nick){
  for (auto const& [key, user] : users) {
    if (user.nickname == nick) return true;
  }
  return false;
}
