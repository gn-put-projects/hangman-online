#ifndef USER_H
#define USER_H

#include <iostream>

#define ROLE_VOID 0
#define ROLE_VIEWER 1
#define ROLE_PLAYER 2

class User;

#include "Game.h"

class User {
private:
public:
  std::string nickname;
  int score;
  long long time;
  int gallowParts;
  int role;
  int id;
  bool ready;
  bool lost;
  void reset();
  User(int, std::string);
  ~User();
};

#endif
