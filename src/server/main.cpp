#include "Server.h"
#include <signal.h>

bool appRunning;

void sigIntHandle(int) {
  appRunning=false;
}

int main(){
  signal(SIGINT, sigIntHandle);

  appRunning = true;
  
  Server server(9191);

  while (appRunning) {
    server.update();
  }
 	
  return 0;
}
