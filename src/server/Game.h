#ifndef GAME_H
#define GAME_H

#include <iostream>
#include <map>
#include <string>
#include <fstream>
#include <stdlib.h>     /* srand, rand */
#include <ctype.h>      /* tolower */
#include <vector>       /* vectors */
#include <ctime>        /* time */
#include "../shared/messageCodes.h"
#include "../shared/time.h"
#include <nlohmann/json.hpp>    /*json*/
using json = nlohmann::json;

#define START_TIME 30500   /* it is also added when you guess correctly*/
#define PARTS_OF_GALLOWS 11
#define BONUS_TIME 10000
#define LETTER_POINTS 10
#define WORDS_POINTS 100
#define WORDS_IN_TXT 6394
#define COVERED_LETTER '_'

#define UPDATE_PERIOD 480
#define UPDATE_MESSAGE_PERIOD 980

#define LOBBY_START_TIME 15500
#define SCORES_DISPLAY_TIME 15500

#define STATE_LOBBY 0
#define STATE_GAME 1
#define STATE_SCORES 2

class Game;

#include "User.h"
#include "Server.h"

class Game {
private:
  Server* server;
  //round for blocking late players
  int round;
  std::string lastWord;
  std::string word;
  std::string covered_word;
  std::string notification;
  //client -> score, information stays only in times about users paricipation after defeat
  std::map<int, User> users;
  //std::map<std::string, float> times;
  //std::map<std::string, int> gallows;
  bool tried_letters[26];
  std::vector<std::string> used_words;
  long long time;
  // time of last player update message
  long long lastUpdateTime;

  //checks if char exists in word
  bool existsInString(char symbol);
  //inserts all chars into covered string
  void updateCoveredString(char symbol);
  //update player status after defeat
  void playerLost(int id); 
  //checks if word was used
  bool existsInUsed_words(std::string guess);
  //sets next round
  void nextRound();
  std::string getLobbyPlayerList();
  std::string getTop3Ranking();
  std::string getScoresList(int);
  int lettersLeft();
  void userFault(int);
  void letterGuess(int, char);

  bool updateRequested;
  int state;
  void updatePlayers();
  int lostPlayerCount();
  int playerCount();
  int readyPlayerCount();
  int viewerCount();
  int undecidedCount();

  std::string lobbyStatus;
  void updateLobbyStatus(long long);
  long long lobbyTimer;
  bool lobbyCountdown;

  std::string scoresStatus;
  void updateScoresStatus(long long);
  long long scoresTimer;
  bool scoresCountdown;
  
  void startGame();
  void endGame();
public:
  bool gameStarted;
  
  Game(Server*);
  void reset(bool);
  //prepers word and covered_word to game
  void createWord();
  //shows all properties
  void debugDisplay();
  //scores user's guess (in positiv or negativ way)
  void usersGuess(int id, std::string guess);
  //adds new client
  int addUser(int id, std::string);
  //removes a user
  void removeUser(int);
  //checks if any player left
  bool playerLeft();
  //updates the server
  void update();
  //read and handle messages
  void readJson(int id, json j);
  void processMessage(int id, json j);
  json createJson(int id, int type);
  //type of user 0 -> not user, 1 -> player, 2 -> viewer
  bool isNicknameUsed(std::string);

  long long timeToUpdate();
};

#endif
