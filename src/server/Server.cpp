#include "Server.h"

/**
   Process a received message
*/ 
void Server::processMessage(int clientFd, std::string msg) {
  if (msg == "") return;
  this->game->readJson(clientFd, json::parse(msg));
}

Server::Server(int port) {
  sockaddr_in serverAddress {
    .sin_family = AF_INET,
    .sin_port = htons(port),
    .sin_addr = {htonl(INADDR_ANY)},
    .sin_zero = 0
  };
  memset(&serverAddress.sin_zero, 0, sizeof(serverAddress.sin_zero));
  
  this->sock = socket(serverAddress.sin_family, SOCK_STREAM | SOCK_NONBLOCK, 0);
  if (this->sock == -1)
    error(1, errno, "Could not create a socket");

  int reuse = 1;
  setsockopt(this->sock, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse));

  int bindOut = bind(sock, (sockaddr*) &serverAddress, sizeof(serverAddress));
  if (bindOut != 0) {
    if (bindOut == 1)
      std::cout<<"The given address is already in use.\n";
    error(1, errno, "Could not bind the socket");
  }

  int listenOut = listen(sock, 1);
  if (listenOut == -1)
    error(1, errno, "Listen failed");

  this->clients.clear();
  
  this->eFd = epoll_create1(0);

  // An event describing new user trying to connect to the server
  epoll_event connectionEvent;
  connectionEvent.events = EPOLLIN;
  connectionEvent.data.fd = this->sock;
  epoll_ctl(this->eFd, EPOLL_CTL_ADD, this->sock, &connectionEvent);

  // Game initialization
  this->game = std::make_unique<Game>(this);
}

/**
   Returns true if a new client has been accepted.
   False otherwise.
*/
bool Server::acceptClient() {
  //std::cout<<"accepting a connection...\n";
  
  socklen_t peer_addr_size;
  struct sockaddr_in peer_addr;
  peer_addr_size = sizeof(peer_addr);

  int clientFd;
  clientFd = accept(sock, (struct sockaddr*) &peer_addr, &peer_addr_size);
  if (clientFd == -1) {
    if (errno & EWOULDBLOCK) {
      return false;
    }
    error(1, errno, "Accept failed");
  }
  
  //std::cout<<"Accepted client: "<<clientFd<<"\n";

  if (this->clients.contains(clientFd))
    error(1, errno, "A client with duplicate fd connected");
  
  int fcntlOut = fcntl(clientFd, F_SETFL,
		       fcntl(clientFd, F_GETFL, 0) | O_NONBLOCK);

  if (fcntlOut == -1)
    error(1, errno, "Fnctl failed (setting accepted fd to NONBLOCKing mode)");

  epoll_event e;
  e.events = EPOLLIN;
  e.data.fd = clientFd;

  epoll_ctl(this->eFd, EPOLL_CTL_ADD, clientFd, &e);

  this->clients.emplace(std::piecewise_construct,
			std::forward_as_tuple(clientFd),
			std::forward_as_tuple(clientFd, this->eFd));
  
  // Greet the client, inform that the connection has been accepted
  json greeting;
  greeting["type"] = SERVER_USER_ACCEPTED;
    
  clients.at(clientFd).requestWrite(greeting.dump());
  return true;
}

/**
   Send a json message to a specified client
*/
void Server::sendMessage(int clientId, json j) {
  clients.at(clientId).requestWrite(j.dump());
}

void Server::removeClient(int clientFd, bool eraseFromSet=true) {
  if (!this->clients.contains(clientFd))
    error(1, errno, "Attempt to remove a non-registered client");
  
  //std::cout<<"Removing client: "<<clientFd<<"\n";

  this->game->removeUser(clientFd);
  
  if (eraseFromSet)
    this->clients.erase(clientFd);
  
  epoll_ctl(this->eFd, EPOLL_CTL_DEL, clientFd, nullptr);
  
  close(clientFd);

  if (this->clients.empty()) {
    game->reset(true);
  }
}

void Server::update() {
  long long ttu = game->timeToUpdate();
  epoll_event e;
  int n = epoll_wait(this->eFd, &e, 1, ttu);
  if (n < 1) {
    game->update();
    return;
  }
  
  if (e.data.fd == this->sock) {
    if (e.events & EPOLLIN) {
      // Connection request sent by a new client
      this->acceptClient();
      return;
    }
    error(1, errno, "Unrecognized event at main server socket");
  }

  int clientFd = e.data.fd;
  
  if (e.events & EPOLLERR) {
    this->removeClient(clientFd);
    return;
  }

  if (e.events & EPOLLIN) {
    bool read = this->clients.at(clientFd).readNow();

    if (!read) {
      this->removeClient(e.data.fd);
      return;
    }
    
    std::string message = this->clients.at(clientFd).receiveMessage();

    this->processMessage(clientFd, message);
    return;
  }

  if (e.events & EPOLLOUT) {
    this->clients.at(e.data.fd).writeNow();
    return;
  }
  
  std::cout<<"Unrecognized epoll event!\n";
}

Server::~Server() {
  for (const auto& [client, writer] : this->clients) {
    this->removeClient(client, false);
  }
  this->clients.clear();
       
  close(sock);
}
