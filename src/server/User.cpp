#include "User.h"

User::User(int givenId, std::string nick) {
  id = givenId;
  nickname = nick;
  this->reset();
}

void User::reset() {
  score = 0;
  time = START_TIME;
  gallowParts = 0;
  role = ROLE_VOID;
  ready = false;
  lost = false;
}

User::~User() {

}
