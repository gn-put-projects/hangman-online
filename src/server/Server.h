#ifndef SERVER_H
#define SERVER_H

#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <error.h>
#include <netdb.h>
#include <sys/epoll.h>
#include <poll.h> 
#include <thread>
#include <vector>
#include <map>
#include "../shared/net.h"
#include "../shared/ReadWriter.h"
#include "../shared/messageCodes.h"
#include <memory>

#include <nlohmann/json.hpp>
using json = nlohmann::json;

class Server;

#include "Game.h"

class Server {
private:
  int sock;
  int eFd;
  std::map<int, ReadWriter> clients;
  std::unique_ptr<Game> game;
public:
  Server(int);
  
  /*
   * Determines whether the server is running.
   */
  bool running;
  bool acceptClient();
  void removeClient(int, bool);
  void update();
  void sendMessage(int, json);
  void processMessage(int, std::string);
  ~Server();
};

#endif
